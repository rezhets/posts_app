import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:postsapp/models/models.dart';
import 'package:postsapp/support/constants.dart';

class DatabaseService {

  static Future initHive() async {
    await Hive.initFlutter();
    Hive.registerAdapter(PostModelAdapter());
    Hive.registerAdapter(CommentModelAdapter());
    Hive.registerAdapter(UserModelAdapter());
    var dir = await getApplicationDocumentsDirectory();
    Hive.init(dir.path);
    await Hive.openBox(HiveBoxes.posts);
    await Hive.openBox(HiveBoxes.comments);
    await Hive.openBox(HiveBoxes.users);
  }

  static final DatabaseService _instance = DatabaseService._internal();

  factory DatabaseService() {
    return _instance;
  }

  DatabaseService._internal();

  void addPostsToDb(Future<List<PostModel>> posts) async {
    posts.then((posts){
      Hive.box(HiveBoxes.posts).put('posts', posts);
      print('DB Service: posts added');
    }).catchError((e){
      print('DB Error: posts was not added');
    });
  }

  Future<List<PostModel>> fetchPostsFromDb() async {
    var posts = Hive.box(HiveBoxes.posts).get('posts', defaultValue: []).cast<PostModel>();
    print('DB Service: fetch ${posts.length} posts');
    return posts;
  }


  void addCommentsToDb(Future<List<CommentModel>> comments, int postId) async {
    comments.then((comments){
      Hive.box(HiveBoxes.comments).put(postId, comments);
      print('DB Service: comments for post $postId added');
    }).catchError((e){
      print('DB Error: comments for post $postId was not added');
    });
  }

  Future<List<CommentModel>> fetchCommentsFromDb(int postId) async {
    var comments = Hive.box(HiveBoxes.comments).get(postId, defaultValue: []).cast<CommentModel>();
    print('DB Service: fetch ${comments.length} comments for post: $postId');
    return comments;
  }

  void addUsersToDb(Future<List<UserModel>> users) async {
    users.then((users){
      Hive.box(HiveBoxes.users).put('users', users);
      print('DB Service: users added');
    }).catchError((e){
      print('DB Error: users was not added, $e');
    });
  }

  Future<List<UserModel>> fetchUsersFromDb() async {
    var users = Hive.box(HiveBoxes.users).get('users', defaultValue: []).cast<UserModel>();
    print('DB Service: fetch ${users.length} users');
    return users;
  }
}
