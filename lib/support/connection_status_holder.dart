class ConnectionStatus {
  static final ConnectionStatus _shared =
      ConnectionStatus._internal();

  factory ConnectionStatus() {
    return _shared;
  }

  bool isOnline = true;

  ConnectionStatus._internal();
}
