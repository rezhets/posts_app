import 'package:flutter/material.dart';

class TextStyles {
  static TextStyle postTitle = TextStyle(fontSize: 18, fontWeight: FontWeight.w500, color: Color(0xff546e7a));
  static TextStyle postBody = TextStyle(fontSize: 14, fontWeight: FontWeight.w300);
  static TextStyle postAuthor = TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Color(0xff546e7a));
  static TextStyle commentTitle = TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: Color(0xff546e7a));
  static TextStyle commentBody = TextStyle(fontSize: 12, fontWeight: FontWeight.w300);
  static TextStyle appBarTitle = TextStyle(fontSize: 22, fontWeight: FontWeight.w300);
  static TextStyle connectionStatus = TextStyle(fontSize: 10, fontWeight: FontWeight.w300, color: Colors.white60);
  static TextStyle splash = TextStyle(fontSize: 32, color: Colors.blueGrey, fontWeight: FontWeight.w500);
}

class HiveBoxes {
  static String posts = 'postsBox';
  static String comments = 'commentsBox';
  static String users = 'usersBox';
}


class Urls {
  static String getPostsUrl = 'https://jsonplaceholder.typicode.com/posts';
  static String getCommentsUrl = 'https://jsonplaceholder.typicode.com/comments?postId=';
  static String getUsersUrl = 'https://jsonplaceholder.typicode.com/users';
}