import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:postsapp/connectivity_bloc/bloc.dart';
import 'package:postsapp/support/constants.dart';
import 'package:postsapp/connectivity_bloc/connectivity_bloc.dart';

class InteractiveAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  Size get preferredSize => Size.fromHeight(56);

  final String title;
  final Function refresh;

  InteractiveAppBar({@required this.title, this.refresh});

  @override
  Widget build(BuildContext context) {
    return BlocListener<ConnectivityBloc, ConnectivityState>(
      listener: (context, state) {
        if (state is Online && ModalRoute.of(context).isCurrent){
          refresh();
        }
      },
      child: BlocBuilder<ConnectivityBloc, ConnectivityState>(
        bloc: BlocProvider.of<ConnectivityBloc>(context),
        builder: (BuildContext context, ConnectivityState state) {

          return AppBar(
            title: Text(
              title,
              style: TextStyles.appBarTitle,
            ),
            actions: <Widget>[
              Visibility(
                visible: state is Offline,
                child: Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Text('Offline mode\nThe data may not be \nup to date',
                        style: TextStyles.connectionStatus)),
              ),
              FlatButton(
                  child: Icon(
                    Icons.refresh,
                    color: (state is Online && refresh != null) ? Colors.white : Colors.white30,
                  ),
                  onPressed: state is Online ? refresh : null)
            ],
          );
        },
      ),
    );
  }
}
