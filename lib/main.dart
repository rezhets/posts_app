import 'package:flutter/material.dart';
import 'package:postsapp/connectivity_bloc/bloc.dart';
import 'package:postsapp/screens/home_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() async {
  runApp(PostsApp());
}

class PostsApp extends StatefulWidget {
  @override
  _PostsAppState createState() => _PostsAppState();
}

class _PostsAppState extends State<PostsApp> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<ConnectivityBloc>(
      create: (context) => ConnectivityBloc(),
      child: MaterialApp(
          theme: Theme.of(context).copyWith(
              primaryColor: Colors.blueGrey,
              primaryColorLight: Colors.blueGrey.shade300),
          home: HomeScreen()),
    );
  }
}
