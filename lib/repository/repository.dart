import 'package:postsapp/database/database_service.dart';
import 'package:postsapp/models/models.dart';
import 'package:postsapp/networking/network_service.dart';
import 'package:postsapp/support/connection_status_holder.dart';

class Repository {

  final NetworkService networkService;
  final DatabaseService dbService;
  Repository({this.networkService, this.dbService});


  Future<List<PostModel>> fetchPosts() async {
    if (!ConnectionStatus().isOnline) {
      return dbService.fetchPostsFromDb();
    }
    var postsFromNetwork = networkService.fetchPostsFromNetwork();
    dbService.addPostsToDb(postsFromNetwork);
    return postsFromNetwork;
  }

  Future<List<CommentModel>> fetchComments(int postId) async {
    if (!ConnectionStatus().isOnline) {
      return dbService.fetchCommentsFromDb(postId);
    }
    var commentsFromNetwork = networkService.fetchCommentsFromNetwork(postId);
    dbService.addCommentsToDb(commentsFromNetwork, postId);
    return commentsFromNetwork;
  }

  Future<List<UserModel>> fetchUsers() async {
    if (!ConnectionStatus().isOnline) {
      return dbService.fetchUsersFromDb();
    }
    var usersFromNetwork = networkService.fetchUsersFromNetwork();
    dbService.addUsersToDb(usersFromNetwork);
    return usersFromNetwork;
  }
}
