import 'dart:convert';
import 'package:http/http.dart' as http show get;
import 'package:postsapp/models/models.dart';
import 'package:postsapp/support/constants.dart';

class NetworkService {

  static final NetworkService _instance = NetworkService._internal();

  factory NetworkService() {
    return _instance;
  }

  NetworkService._internal();

  Future<List<PostModel>> fetchPostsFromNetwork() async {

    List<PostModel> posts = [];
    print('NetworkService: Getting posts');

    try {
      var data = await _getRequest(Urls.getPostsUrl);
      for (var parsedJson in data) {
        PostModel post = PostModel.fromJson(parsedJson);
        posts.add(post);
      }
      print('NetworkService: ${posts.length} posts received');
      return posts;
    } catch (e) {
      String error = ('Can\'t load posts\n$e');
      print ('NetworkService error: $error');
      throw (error);
    }
  }

  Future<List<CommentModel>> fetchCommentsFromNetwork(int postId) async {

    List<CommentModel> comments = [];
    print('NetworkService: Getting comments for post $postId');

    try {
      var data = await _getRequest('${Urls.getCommentsUrl}$postId');
      for (var parsedJson in data) {
        CommentModel comment = CommentModel.fromJson(parsedJson);
        comments.add(comment);
      }
      print('NetworkService: ${comments.length} comments for post $postId received');
      return comments;
    } catch (e) {
      String error = ('Can\'t load comments for post $postId\n$e');
      print('NetworkService error: $error');
      throw (error);
    }
  }

  Future<List<UserModel>> fetchUsersFromNetwork() async {

    print('NetworkService: Getting users');

    List<UserModel> users = [];

    try {
      var data = await _getRequest(Urls.getUsersUrl);
      for (var parsedJson in data) {
        UserModel user = UserModel.fromJson(parsedJson);
        users.add(user);
      }
      print('NetworkService: ${users.length} users received');
      return users;
    } catch (e) {
      String error = ('Can\'t load users\n$e');
      print('NetworkService error: $error');
      throw (error);
    }
  }

  Future _getRequest(String url) async {
    try {
      var response = await http
          .get(url, headers: {'Accept': 'application/json'}).timeout(
              Duration(seconds: 20), onTimeout: () {
        throw ('Request timeout');
      });
      var data = jsonDecode(response.body);
      return data;
    } catch (e) {
      print('NetworkService error: $e');
      throw (e);
    }
  }
}


