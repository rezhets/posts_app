import 'dart:async';
import 'package:bloc/bloc.dart';
import './bloc.dart';

class ConnectivityBloc extends Bloc<ConnectivityEvent, ConnectivityState> {
  @override
  ConnectivityState get initialState => InitialConnectivityState();

  @override
  Stream<ConnectivityState> mapEventToState(
    ConnectivityEvent event,
  ) async* {
    if (event is ChangeOnlineStatus){
      print ('Connectivity: state changed, isOnline = ${event.isOnline}');
      yield event.isOnline ? Online() : Offline();
    }
  }
}
