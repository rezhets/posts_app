import 'package:equatable/equatable.dart';

abstract class ConnectivityState extends Equatable {
  const ConnectivityState();
  @override
  List<Object> get props => [];
}

class InitialConnectivityState extends ConnectivityState {
  @override
  List<Object> get props => [];
}
class Online extends ConnectivityState  {}
class Offline extends ConnectivityState {}


