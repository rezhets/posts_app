import 'package:equatable/equatable.dart';

abstract class ConnectivityEvent extends Equatable {
  const ConnectivityEvent();
}

class ChangeOnlineStatus extends ConnectivityEvent {
  final bool isOnline;
  ChangeOnlineStatus(this.isOnline);
  @override
  List<Object> get props => [isOnline];
}