import 'package:flutter/material.dart';
import 'package:postsapp/support/constants.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('PostsApp', style: TextStyles.splash)
      ),
    );
  }
}
