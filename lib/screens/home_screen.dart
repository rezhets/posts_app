import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:postsapp/connectivity_bloc/bloc.dart';
import 'package:postsapp/database/database_service.dart';
import 'package:postsapp/screens/posts/posts_screen.dart';
import 'package:postsapp/screens/splash_screen.dart';
import 'package:postsapp/support/connection_status_holder.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool _isSettingUp = true;
  var _connectivitySubscription;

  @override
  void initState() {
    setupApp();
    super.initState();
  }

  setupApp() async {
    await _initDatabase();
    await _setupConnectivity();

    setState(() {
      _isSettingUp = false;
    });
  }

  Future _initDatabase() async {
    return DatabaseService.initHive();
  }

  Future _setupConnectivity() async {
    bool isOnline =  (await Connectivity().checkConnectivity() != ConnectivityResult.none);
    ConnectionStatus().isOnline = isOnline;

    BlocProvider.of<ConnectivityBloc>(context).add(ChangeOnlineStatus(isOnline));

    _connectivitySubscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      bool isOnline = result != ConnectivityResult.none;
      if (ConnectionStatus().isOnline != isOnline) {
        ConnectionStatus().isOnline = isOnline;
        BlocProvider.of<ConnectivityBloc>(context)
            .add(ChangeOnlineStatus(isOnline));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return _isSettingUp ? SplashScreen() : PostsScreen();
  }

  @override
  dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }
}
