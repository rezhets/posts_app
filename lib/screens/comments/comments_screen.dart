import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:postsapp/models/post_model.dart';
import 'package:postsapp/screens/comments/widgets/comments_content.dart';
import 'package:postsapp/common_widgets/loading_overlay.dart';
import 'package:postsapp/common_widgets/interactive_app_bar.dart';
import 'comments_bloc/bloc.dart';

class CommentsScreen extends StatelessWidget {
  final PostModel post;
  CommentsScreen({@required this.post});
  @override
  Widget build(BuildContext context) {
    return BlocProvider<CommentsBloc>(
      create: (context) => CommentsBloc(post),
      child: CommentsScreenContainer(),
    );
  }
}

class CommentsScreenContainer extends StatefulWidget {
  @override
  CommentsScreenContainerState createState() => CommentsScreenContainerState();
}

class CommentsScreenContainerState extends State<CommentsScreenContainer> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<CommentsBloc>(context).add(GetComments());
  }

  @override
  Widget build(BuildContext context) {

    return BlocListener<CommentsBloc, CommentsState>(
      listener: (BuildContext context, CommentsState state) {
        if (state is CommentsError) {
          _showError(state.error);
        }
      },
      child: BlocBuilder<CommentsBloc, CommentsState>(
        bloc: BlocProvider.of<CommentsBloc>(context),
        builder: (BuildContext context, CommentsState state) {
          bool isLoading = (state is CommentsLoading);
          return Scaffold(
            appBar: InteractiveAppBar(
              title: 'Comments',
              refresh: isLoading ? null : () {
                      BlocProvider.of<CommentsBloc>(context).add(GetComments());
                    },
            ),
            body: LoadingOverlay(
              color: Theme.of(context).primaryColorLight,
              isLoading: isLoading,
              child: CommentsContent(),
            ),
          );
        },
      ),
    );
  }

  void _showError(String error) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              'Error',
              textAlign: TextAlign.center,
            ),
            content: Text(error),
          );
        });
  }
}
