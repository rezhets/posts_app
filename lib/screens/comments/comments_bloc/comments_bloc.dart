import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:postsapp/database/database_service.dart';
import 'package:postsapp/models/models.dart';
import 'package:postsapp/networking/network_service.dart';
import 'package:postsapp/repository/repository.dart';

import 'bloc.dart';

class CommentsBloc extends Bloc<CommentsEvent, CommentsState> {
  final PostModel post;
  CommentsBloc(this.post);

  Repository _repository = Repository(networkService: NetworkService(), dbService: DatabaseService());

  @override
  CommentsState get initialState => InitialCommentsState();

  @override
  Stream<CommentsState> mapEventToState(
    CommentsEvent event,
  ) async* {
    if (event is GetComments) {
      yield CommentsLoading();
      List<CommentModel> comments = [];
      try {
        comments = await _repository.fetchComments(this.post.id);
        yield CommentsLoaded(comments);
      } catch (e) {
        yield CommentsError(e.toString());
      }
    }
  }
}
