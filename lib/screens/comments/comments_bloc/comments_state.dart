import 'package:equatable/equatable.dart';
import 'package:postsapp/models/comment_model.dart';

abstract class CommentsState extends Equatable {
  const CommentsState();
  @override
  List<Object> get props => [];
}

class InitialCommentsState extends CommentsState {}

class CommentsLoading extends CommentsState {}

class CommentsLoaded extends CommentsState {
  final List<CommentModel> comments;
  CommentsLoaded(this.comments);
  @override
  List<Object> get props => [comments];
}

class CommentsError extends CommentsState {
  final String error;
  CommentsError(this.error);
  @override
  List<Object> get props => [error];
}
