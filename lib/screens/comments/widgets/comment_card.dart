import 'package:flutter/material.dart';
import 'package:postsapp/models/comment_model.dart';
import 'package:postsapp/support/constants.dart';

class CommentCard extends StatelessWidget {
  final CommentModel comment;
  CommentCard(this.comment);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Text(
            comment.name,
            style: TextStyles.commentTitle,
            textAlign: TextAlign.center,
          ),
          Padding(padding: EdgeInsets.symmetric(horizontal: 0, vertical: 5),
            child: Text(
              comment.body,
              style: TextStyles.commentBody,
            ),
          ),
          Text('by: ${comment.email}', textAlign: TextAlign.right, style: TextStyles.commentBody,),
          SizedBox(
            height: 5,
          ),
          Container(width: double.infinity, height: 0.5, color: Theme.of(context).primaryColor)
        ],
      ),
    );
  }
}
