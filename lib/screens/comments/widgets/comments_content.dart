import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:postsapp/models/comment_model.dart';
import 'package:postsapp/screens/comments/comments_bloc/bloc.dart';
import 'package:postsapp/screens/posts/widgets/post_card.dart';
import 'package:postsapp/screens/comments/widgets/comment_card.dart';
import 'no_comments_yet.dart';

class CommentsContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CommentsBloc, CommentsState>(
        bloc: BlocProvider.of<CommentsBloc>(context),
        condition: (previousState, newState) {
          return newState is CommentsLoaded;
        },
        builder: (BuildContext context, CommentsState state) {
          return Column(
            children: <Widget>[
              DecoratedPostCard(),
              state is CommentsLoaded
                  ? CommentsList(state.comments)
                  : Container()
            ],
          );
        });
  }

}

class DecoratedPostCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(color: Theme.of(context).primaryColor)),
      child: PostCard(
        post: BlocProvider.of<CommentsBloc>(context).post,
      ),
    );
  }
}


class CommentsList extends StatelessWidget {
  final List<CommentModel> comments;
  CommentsList(this.comments);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: comments.isEmpty
          ? NoCommentsYet()
          : ListView.builder(
              itemCount: comments.length,
              itemBuilder: (BuildContext context, int index) {
                return CommentCard(comments[index]);
              }),
    );
  }
}
