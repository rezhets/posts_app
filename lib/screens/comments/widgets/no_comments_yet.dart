import 'package:flutter/material.dart';

class NoCommentsYet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 20),
      child: Container(
        child: Text('THERE ARE NO COMMENTS YET',
            style: Theme.of(context).textTheme.subtitle1),
      ),
    );
  }
}
