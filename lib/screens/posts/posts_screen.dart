import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:postsapp/common_widgets/loading_overlay.dart';
import 'package:postsapp/common_widgets/interactive_app_bar.dart';
import 'package:postsapp/screens/posts/posts_bloc/bloc.dart';
import 'package:postsapp/screens/posts/widgets/posts_content.dart';

class PostsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<PostsBloc>(
        create: (context) => PostsBloc(), child: PostsScreenContainer());
  }
}

class PostsScreenContainer extends StatefulWidget {
  @override
  PostsScreenContainerState createState() => new PostsScreenContainerState();
}

class PostsScreenContainerState extends State<PostsScreenContainer> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<PostsBloc>(context).add(GetPosts());
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<PostsBloc, PostsState>(
      listener: (BuildContext context, PostsState state) {
        if (state is PostsError) {
          _showError(state.error);
        }
      },
      child: BlocBuilder<PostsBloc, PostsState>(
          bloc: BlocProvider.of<PostsBloc>(context),
          builder: (BuildContext context, PostsState state) {
            bool isLoading = (state is PostsLoading);
            return Scaffold(
                appBar: InteractiveAppBar(
                    title: 'Posts',
                    refresh:  isLoading ? null : () {
                      BlocProvider.of<PostsBloc>(context).add(GetPosts());
                    }),
                body: LoadingOverlay(
                  isLoading: state is PostsLoading,
                  color: Theme.of(context).primaryColorLight,
                  child: PostsContent(),
                ));
          }),
    );
  }

  void _showError(String error) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              'Error',
              textAlign: TextAlign.center,
            ),
            content: Text(error),
          );
        });
  }
}
