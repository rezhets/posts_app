import 'package:flutter/material.dart';

class NothingToShow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Text('Nothing to show',
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headline5),
        Padding(
            padding: EdgeInsets.symmetric(vertical: 30),
            child: Icon(Icons.error,
                color: Theme.of(context).primaryColor, size: 60)),
        Text(
          'Check your internet connection',
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.headline5,
        ),
      ],
    );
  }
}
