import 'package:flutter/material.dart';
import 'package:postsapp/models/post_model.dart';
import 'package:postsapp/support/constants.dart';

class PostCard extends StatelessWidget {
  final PostModel post;
  final Function onPressed;

  PostCard({this.post, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      disabledColor: Colors.white,
      disabledTextColor: Colors.black,
      elevation: 15,
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        color: Colors.white,
        onPressed: onPressed,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text(
              post.title,
              style: TextStyles.postTitle,
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 10,),
            Text(
              post.body,
              style: TextStyles.postBody,
              textAlign: TextAlign.start,
            ),
            SizedBox(height: 5,),
            Text(
              'by ${post.username}',
              style: TextStyles.postAuthor,
              textAlign: TextAlign.end,
            ),
          ],
        ));
  }
}
