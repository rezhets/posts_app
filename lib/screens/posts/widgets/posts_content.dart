import 'package:flutter/material.dart';
import 'package:postsapp/models/post_model.dart';
import 'package:postsapp/screens/comments/comments_screen.dart';
import 'package:postsapp/screens/posts/widgets/nothing_to_show.dart';
import 'package:postsapp/screens/posts/posts_bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'post_card.dart';

class PostsContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PostsBloc, PostsState>(
        bloc: BlocProvider.of<PostsBloc>(context),
        condition: (previousState, newState) {
          return newState is PostsLoaded;
        },
        builder: (BuildContext context, PostsState state) {
          if (state is PostsLoaded) {
            return state.posts.isEmpty
                ? NothingToShow()
                : PostsList(state.posts);
          }
          return Container();
        });
  }
}

class PostsList extends StatelessWidget {
  final List<PostModel> posts;
  PostsList(this.posts);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: posts.length,
        itemBuilder: (BuildContext context, int index) {
          PostModel post = posts[index];
          return PostCard(
            post: post,
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => CommentsScreen(
                      post: post,
                    ),
                  ));
            },
          );
        });
  }
}
