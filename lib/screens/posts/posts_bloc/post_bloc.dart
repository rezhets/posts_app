import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:postsapp/database/database_service.dart';
import 'package:postsapp/models/models.dart';
import 'package:postsapp/networking/network_service.dart';
import 'package:postsapp/repository/repository.dart';

import 'bloc.dart';

class PostsBloc extends Bloc<PostsEvent, PostsState> {

  @override
  PostsState get initialState => InitialBlocState();
  Repository _repository = Repository(networkService: NetworkService(), dbService: DatabaseService());

  @override
  Stream<PostsState> mapEventToState(
    PostsEvent event,
  ) async* {
    if (event is GetPosts) {
      yield PostsLoading();
      List<PostModel> posts = [];
      List<UserModel> users = [];
      try {
        await Future.wait([_repository.fetchPosts(), _repository.fetchUsers()]).then((result){
          posts = result[0];
          users = result[1];
          Map<int, UserModel> usersMap = Map.fromIterable(users, key: (user) => user.id, value: (user) => user);
          posts.forEach((post) => post.username = usersMap[post.userId].username);
        });
       // yield PostsError('fake error');
        yield PostsLoaded(posts);
      } catch (e) {
        yield PostsError(e.toString());
      }
    }





  }


}
