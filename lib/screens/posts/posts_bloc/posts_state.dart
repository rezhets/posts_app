import 'package:equatable/equatable.dart';
import 'package:postsapp/models/post_model.dart';

abstract class PostsState extends Equatable {
  const PostsState();
  @override
  List<Object> get props => [];
}

class InitialBlocState extends PostsState {
  @override
  List<Object> get props => [];
}

class PostsLoading extends PostsState {}

class PostsLoaded extends PostsState {
  final List<PostModel> posts;
  PostsLoaded(this.posts);
  @override
  List<Object> get props => [posts];
}

class PostsError extends PostsState {
  final String error;
  PostsError(this.error);
  @override
  List<Object> get props => [error];
}

