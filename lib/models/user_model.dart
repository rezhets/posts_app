import 'dart:math';

import 'package:postsapp/models/comment_model.dart';
import 'package:hive/hive.dart';
part 'user_model.g.dart';

@HiveType(typeId: 3)
class UserModel extends HiveObject {

  @HiveField(0)
  int id;
  @HiveField(1)
  String name;
  @HiveField(2)
  String username;
  @HiveField(3)
  String email;
  @HiveField(4)
  List<CommentModel> comments = [];

  UserModel({this.id, this.name, this.username, this.email});

  UserModel.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson['id'];
    name = parsedJson['name'];
    username = parsedJson['username'];
    email = parsedJson['email'];
  }

  @override
  String toString() {
    return '<USER: $id, $username>';
  }
}
