import 'package:hive/hive.dart';
part 'comment_model.g.dart';

@HiveType(typeId: 1)
class CommentModel extends HiveObject{

  @HiveField(0)
  int id;
  @HiveField(1)
  int postId;
  @HiveField(2)
  String name;
  @HiveField(3)
  String email;
  @HiveField(4)
  String body;

  CommentModel({this.id, this.postId, this.name, this.email, this.body});

  CommentModel.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson['id'];
    postId = parsedJson['postId'];
    name = parsedJson['name'];
    email = parsedJson['email'];
    body = parsedJson['body'];
  }

  @override
  String toString() {
    return 'Comment: $id, $name';
  }
}
