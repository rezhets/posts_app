// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PostModelAdapter extends TypeAdapter<PostModel> {
  @override
  final typeId = 2;

  @override
  PostModel read(BinaryReader reader) {
    var numOfFields = reader.readByte();
    var fields = <int, dynamic>{
      for (var i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PostModel(
      id: fields[0] as int,
      userId: fields[1] as int,
      title: fields[2] as String,
      body: fields[3] as String,
    )
      ..username = fields[4] as String
      ..comments = (fields[5] as List)?.cast<CommentModel>();
  }

  @override
  void write(BinaryWriter writer, PostModel obj) {
    writer
      ..writeByte(6)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.userId)
      ..writeByte(2)
      ..write(obj.title)
      ..writeByte(3)
      ..write(obj.body)
      ..writeByte(4)
      ..write(obj.username)
      ..writeByte(5)
      ..write(obj.comments);
  }
}
