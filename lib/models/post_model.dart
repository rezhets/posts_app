import 'package:postsapp/models/comment_model.dart';
import 'package:hive/hive.dart';
part 'post_model.g.dart';

@HiveType(typeId: 2)
class PostModel extends HiveObject {

  @HiveField(0)
  int id;
  @HiveField(1)
  int userId;
  @HiveField(2)
  String title;
  @HiveField(3)
  String body;
  @HiveField(4)
  String username;
  @HiveField(5)
  List<CommentModel> comments = [];

  PostModel({this.id, this.userId, this.title, this.body});

  PostModel.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson['id'];
    userId = parsedJson['userId'];
    title = parsedJson['title'];
    body = parsedJson['body'];
  }

  @override
  String toString() {
    return '<POST $id, $title, $username>';
  }
}
